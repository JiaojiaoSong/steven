/**
 * Created by Song on 8/31/16.
 */

/**
 * Target Google Sheet IDs
 */
var APIKEY = 'AIzaSyCF4AGB9hMgR5YQ5ykgpG_vhRzXpblqw58';
var SHEETID1 = '1Kkd6MWeZNQ4EAMmb7vus8EtSzxmLCyQ0cly_4mGHIOU';
var aboutsheet='1mCiD60K7q1vILu3QKF2s09JtWNJ4TyQXBXyuqr6nrPg';
var homepage='1JXI-Rxt2HZJyTRin7u69DyHKhAvv9UjxOCcxUduVZHg';
/**
 * Load Sheets API client library.
 */

/**
 * for home page
 */
function loadSheetsApi1() {
    var discoveryUrl =
        'https://sheets.googleapis.com/$discovery/rest?version=v4';
    gapi.client.setApiKey(APIKEY);
    gapi.client.load(discoveryUrl);

    //if local storage has got the data
    if (localStorage && localStorage.getItem("talk")) {
        var talk = JSON.parse(localStorage.getItem("talk"));
        var section1=[];
        var section2=[];
        var whole_section1="";
        var whole_section12="";
        var whole_section2="";
        var whole_section3="";
        var whole_section4="";
        for(var i=0;i<3;i++){
         section1[i]="<div class=''><div class='col-md-5 col-xs-5 text-left ' style='margin-left:5%;'><p class='date'>"+talk[0][i]+"</p></div><div class='col-md-6 col-xs-6 col-sm-6 ' style='padding-left:0.6em'><div class='row text-left'><p class='talktheme'>"+talk[1][i]+"</p></div><div class='row'><p class='location'>"+talk[2][i]+"</p></div></div></div>";
         section2[i]="<div class='row'><div class='col-xs-5'><p class='date'>"+talk[0][i]+"</p></div><div class='col-xs-6 col-xs-offset-1'><div class='row text-left'><p class='talktheme' style='margin-left: -30px'>"+talk[1][i]+"+ </p></div><div class='row text-left'><p class='location'>"+talk[2][i]+"</p></div></div></div>";
         whole_section1+=section1[i];
         whole_section12+=section2[i];
        }
        document.getElementById("talk1").innerHTML=whole_section1;
        document.getElementById("talk2").innerHTML=whole_section12;

    }
    //if not, run the function to request data from Google sheet
    gapi.client.load(discoveryUrl).then(talkfunc);


    if (localStorage && localStorage.getItem("news1")) {
        var news1 = JSON.parse(localStorage.getItem("news1"));
        var section2=[];
        for(var i=0;i<5;i++){

            section2[i]="<span class='season'>"+news1[0][i]+" :  </span>"+news1[1][i]+".<br>";
            whole_section2+=section2[i];
        }
        document.getElementById("news1").innerHTML=whole_section2;
        document.getElementById("news2").innerHTML=whole_section2;
    }
        gapi.client.load(discoveryUrl).then(newsfunc1);

    if (localStorage && localStorage.getItem("homepage")) {
        var homepage = JSON.parse(localStorage.getItem("homepage"));
        document.getElementById('about').innerHTML = homepage["about"];
        document.getElementById('about1').innerHTML = homepage["about"];
        document.getElementById('positionpc').innerHTML = homepage["position"];
        document.getElementById('positionphone').innerHTML = homepage["position"];
        document.getElementById('department').innerHTML =homepage["department"];
        document.getElementById('department1').innerHTML =homepage["department"];
        document.getElementById('emailtextpc').innerHTML = homepage["emailtext"];
        document.getElementById('emailtextphone').innerHTML = homepage["emailtext"];
        document.getElementById('addresstext').innerHTML = homepage["addresstext"];
        document.getElementById('sponsorship').innerHTML = homepage["sponsorship"];
        document.getElementById('sponsorship1').innerHTML = homepage["sponsorship"];
    }
    gapi.client.load(discoveryUrl).then(homefunc1);


    if (localStorage && localStorage.getItem("publication")) {
        var publication = JSON.parse(localStorage.getItem("publication"));
        var section3=[];
        var section4=[];
        for(i=0;i<12;i++){
            if(publication[5][i]) {
                if (publication[3][i]) {
                    if(publication[4][i]){
                        section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + publication[3][i] + " ' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a><a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";

                        section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + publication[3][i] + "' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a><a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                    }
                    else{
                        section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + publication[3][i] + " ' class='pubtitle'>" +publication[0][i] + "</a><br>" + publication[0][i] + "<a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";

                        section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + publication[3][i] + "' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[0][i] + "<a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                    }
                }
                else {
                    if(publication[4][i]){
                        section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a><a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                        section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a><a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                    }
                    else{
                        section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                        section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + publication[5][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[5][i] + "'>video.</a></div> </div>";
                    }
                }
            }

            else {
                if (publication[3][i]) {
                    if (publication[4][i]) {
                            section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + publication[3][i] + " ' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a></div> </div>";
                            section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + publication[3][i] + "' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a></div> </div>";
                        }
                        else {
                            section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + publication[3][i] + " ' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "</div> </div>";

                            section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + publication[3][i] + "' class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "</div> </div>";
                        }
                    }
                    else {
                        if (publication[4][i]) {
                            section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a></div> </div>";

                            section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "<a class='acm' href='" + publication[4][i] + "'>cit.</a></div> </div>";
                        }
                        else {
                            section3[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + publication[2][i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "</div> </div>";

                            section4[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + publication[0][i] + "</a><br>" + publication[1][i] + "</div> </div>";
                        }
                    }
                }whole_section3+=section3[i];
            whole_section4+=section4[i];
        }
        document.getElementById("pubcont").innerHTML=whole_section3;
        document.getElementById("pubcont2").innerHTML=whole_section4;
    }
    gapi.client.load(discoveryUrl).then(publicationfunc);


    if (localStorage && localStorage.getItem("logo")) {
        var logo = JSON.parse(localStorage.getItem("logo"));
        var section=[];
        var whole_section=[];
        for(i=0;i<logo[0].length;i++){
            section[i]="<a href='"+logo[1][i]+"'><img src='"+logo[0][i]+"' width='"+logo[2][i]+"' border='0' style='margin-bottom:20px'></a><br>";
            whole_section+=section[i];
            }
        document.getElementById("logo").innerHTML=whole_section;
        document.getElementById("logo1").innerHTML=whole_section;
    }
    gapi.client.load(discoveryUrl).then(logofunc);

    if (localStorage && localStorage.getItem("talkpage")) {
        var talkpage = JSON.parse(localStorage.getItem("talkpage"));
        var section5=[];
        var whole_section5=[];
        var whole_section6="";
        var a=0;
        for(var y=talkpage[3][0];y>talkpage[3][talkpage[3].length-1]-1;y--){
            whole_section5[a]="<div class='col-md-1 col-md-offset-3  col-sm-1 col-sm-offset-3 col-xs-2 col-xs-offset-1'><p class='year'>"+y+"</p></div><div class='col-md-8 col-sm-8 col-xs-8'  style='margin-bottom: 20px'>";
            for (i=0;i<talkpage[0].length;i++){
                if (talkpage[3][i]==y){
                    section5[i]="<p class='date2' style='margin-top: 5px;padding-left:10px'>"+talkpage[0][i]+"</p><p class='talkstheme' style='display: inline-block;padding-left:20px;'>"+talkpage[1][i]+"</p><p class='location' style='display: inline-block;padding-left:10px;' >"+talkpage[2][i]+"</p><br>";
                    whole_section5[a]+=section5[i];

                }
            }
            whole_section5[a] = whole_section5[a] +"</div>";
            whole_section6+=whole_section5[a];
            a++;
        }
        document.getElementById("talkpage").innerHTML=whole_section6;
        // document.getElementById("talkpage1").innerHTML=whole_section6;
        // document.getElementById("talkpagephone").innerHTML=whole_section6;
    }

    gapi.client.load(discoveryUrl).then(talkpage1);

}



/**
 * for about page
 */
function loadSheetsApi2() {
    var discoveryUrl =
        'https://sheets.googleapis.com/$discovery/rest?version=v4';
    gapi.client.setApiKey(APIKEY);
    gapi.client.load(discoveryUrl);

    if (localStorage && localStorage.getItem("biography")) {
       var biography = JSON.parse(localStorage.getItem("biography"))
       document.getElementsByClassName('overview').innerHTML = biography["overview"];
       document.getElementById('bio').innerHTML = biography["bio"];
        // document.getElementById('overview1').innerHTML = biography["overview"];
        document.getElementById('bio1').innerHTML = biography["bio"];
        }
        gapi.client.load(discoveryUrl).then(biograhy);

}


/**
 * for talk page
 */
function loadSheetsApi3() {
    var discoveryUrl =
        'https://sheets.googleapis.com/$discovery/rest?version=v4';
    gapi.client.setApiKey(APIKEY);
    gapi.client.load(discoveryUrl);
    if (localStorage && localStorage.getItem("talkpage")) {
        var talkpage = JSON.parse(localStorage.getItem("talkpage"));
        var section5=[];
        var whole_section5=[];
        var whole_section6="";
        var a=0;
        for(var y=talkpage[3][0];y>talkpage[3][talkpage[3].length-1]-1;y--){
        whole_section5[a]="<div class='col-md-1 col-md-offset-3  col-sm-1 col-sm-offset-3 col-xs-2 col-xs-offset-1'><p class='year'>"+y+"</p></div><div class='col-md-8 col-sm-8 col-xs-8'>";
          for (i=0;i<talkpage[0].length;i++){
              if (talkpage[3][i]==y){
              section5[i]="<p class='date2' style='margin-top: 5px;'>"+talkpage[0][i]+"</p><p class='talkstheme' style='display: inline-block;margin-left:5px;'>"+talkpage[1][i]+"</p><p class='location' style='display: inline-block;margin-left:5px;' >"+talkpage[2][i]+"</p><br>";
              whole_section5[a]+=section5[i];

            }
        }
        whole_section5[a] = whole_section5[a] +"</div>";
        whole_section6+=whole_section5[a];
        a++;
        }
        // document.getElementById("talkpage").innerHTML=whole_section6;
        document.getElementById("talkpage1").innerHTML=whole_section6;
    }

    gapi.client.load(discoveryUrl).then(talkpage1);

}

function homefunc1() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'information!A2:F'
    }).then(function(response) {
        var range = response.result;
        var value=[];
        for (var i = 0; i < 6; i++)
        {
            var row = range.values[0];
            value[i]=row[i];
        }

        document.getElementById('about').innerHTML = value[0];
        document.getElementById('about1').innerHTML = value[0];
        document.getElementById('positionpc').innerHTML = value[1];
        document.getElementById('positionphone').innerHTML = value[1];
        document.getElementById('department').innerHTML = value[2];
        document.getElementById('department1').innerHTML = value[2];
        document.getElementById('emailtextpc').innerHTML = value[3];
        document.getElementById('emailtextphone').innerHTML = value[3];
        document.getElementById('addresstext').innerHTML = value[4];
        document.getElementById('sponsorship').innerHTML = value[5];
        document.getElementById('sponsorship1').innerHTML = value[5];

        localStorage.setItem("homepage",JSON.stringify({"about": value[0], "position":value[1],"department":value[2],"emailtext":value[3],"addresstext":value[4],"sponsorship":value[5]}));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

/**
 * request for talk
 */
function talkfunc() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'talk!A2:D',
    }).then(function(response) {
      var range = response.result;
        //built array
        var datear=[];
        var themear=[];
        var locationar=[];
        var section=[];
        var section1=[];
        var whole_section1="";
        var whole_section2="";
        //read the first 3 records
        for (var i = 0; i < 3; i++) {
            var row = range.values[i];
            datear[i]=row[0];
            themear[i]=row[1];
            locationar[i]=row[2];
            //inset html
            section[i]="<div class=''><div class='col-md-5 col-xs-5 text-left' style='margin-left: 5%'><p class='date'>"+range.values[i][0]+"</p></div><div class='col-md-6 col-xs-6 col-sm-6 ' style='padding-left:0.6em'><div class='row text-left'><p class='talktheme'>"+range.values[i][1]+"</p></div><div class='row'><p class='location'>"+row[2]+"</p></div></div></div>";

            section1[i]="<div class=''><div class='col-xs-5'><p class='date'>"+range.values[i][0]+"</p></div><div class='col-xs-6 col-xs-offset-1'><div class='row text-left'><p class='talktheme' style='margin-left: -30px'>"+range.values[i][1]+"</p></div><div class='row text-left'><p class='location'>"+row[2]+"</p></div></div></div>";

            whole_section1+=section[i];
            whole_section2+=section1[i];
        }
        document.getElementById("talk1").innerHTML=whole_section1;
        document.getElementById("talk2").innerHTML=whole_section2;
        localStorage.setItem("talk",JSON.stringify([datear,themear,locationar]));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

/**
 * request for publication
 */
function publicationfunc() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'publications!A2:F',
    }).then(function(response) {
        // console.log("here")
        var range = response.result;
        //built array
        var title=[];
        var author=[];
        var images=[];
        var pdf=[];
        var cit=[];
        var video=[];
        var section=[];
        var section1=[];
        var whole_section3="";
        var whole_section4="";

        //read the first 5 records
        for (var i = 0; i < 12; i++) {
            var row = range.values[i];
            title[i]=row[0];
            author[i]=row[1];
            images[i]=row[2];
            pdf[i]=row[3];
            cit[i]=row[4];
            video[i]=row[5];
            //insert HTML
            if(video[i]) {
                if (pdf[i]) {
                    if(cit[i]){
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + pdf[i] + " ' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a><a class='acm' href='" + video[i] + "'>video.</a></div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + pdf[i] + "' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a><a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                    }
                    else{
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + pdf[i] + " ' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + video[i] + "'>video.</a></div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + pdf[i] + "' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                    }
               }
                else {
                   if(cit[i]){
                       section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a><a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                       section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a><a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                   }
                   else{
                       section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                       section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + video[i] + "'>video.</a></div> </div>";
                   }
                }
            }
            else {
                if (pdf[i]) {
                    if (cit[i]) {
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + pdf[i] + " ' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a></div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + pdf[i] + "' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a></div> </div>";
                    }
                    else{
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a href='" + pdf[i] + " ' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "</div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a href='" + pdf[i] + "' class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "</div> </div>";
                    }
                }
                else{
                    if(cit[i]) {
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a></div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "<a class='acm' href='" + cit[i] + "'>cit.</a></div> </div>";
                    }
                    else{
                        section[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-md-2' style='margin-right: 20px;margin-bottom: 10px'><img src='images/" + images[i] + "' width=100 border='1' ></div> <div class='col-md-9'><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "</div> </div>";

                        section1[i] = "<div class='row' style='margin-bottom: 20px;'> <div class='col-xs-12 text-left' ><a class='pubtitle'>" + title[i] + "</a><br>" + author[i] + "</div> </div>";
                    }
                }

            }

            whole_section3+=section[i];
            whole_section4+=section1[i];

        }

        document.getElementById("pubcont").innerHTML=whole_section3;
        document.getElementById("pubcont2").innerHTML=whole_section4;
        localStorage.setItem("publication",JSON.stringify([title,author,images,pdf,cit,video]));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

function logofunc() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'logo!A2:C',
    }).then(function(response) {
        // console.log("here")
        var range = response.result;
        //built array
        var filename=[];
        var href=[];
        var width=[];
        var section=[];
        var whole_section3="";
        for (var i = 0; i < range.values.length; i++) {
            var row = range.values[i];
            filename[i]=row[0];
            href[i]=row[1];
            width[i]=row[2];
            section[i]="<a href="+href[i]+"><img src='"+filename[i]+"' width='"+width[i]+"' border='0' style='margin-bottom:20px'></a><br>";
            whole_section3+=section[i];
        }

        document.getElementById("logo").innerHTML=whole_section3;
        document.getElementById("logo1").innerHTML=whole_section3;
        localStorage.setItem("logo",JSON.stringify([filename,href,width]));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}
/**
 * request for news
 */
function newsfunc1() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'news!A2:B',
    }).then(function(response) {
        // console.log("here")
        var range = response.result;
        //built array
        var datear=[];
        var content=[];
        var section=[];
        var whole_section2="";
        for (var i = 0; i < 5; i++) {
            var row = range.values[i];
            datear[i]=row[0];
            content[i]=row[1];
            section[i]="<span class='season'>"+row[0]+" :  </span>"+row[1]+".<br>";
            whole_section2+=section[i];

        }
        document.getElementById("news1").innerHTML=whole_section2;
        document.getElementById("news2").innerHTML=whole_section2;
        localStorage.setItem("news1",JSON.stringify([datear,content]));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

/**
 * request for biography
 */
function biograhy() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: aboutsheet,
        range: 'about!A2:B',
    }).then(function(response) {
        // console.log("here")
        var range = response.result;
        for (var i = 0; i < range.values.length; i++) {
            var row = range.values[i];
            // document.getElementById('overview').innerHTML = row[0];
            document.getElementById('bio').innerHTML = row[1];
            document.getElementsByClassName('overview').innerHTML = row[0];
            document.getElementById('bio1').innerHTML = row[1];
            localStorage.setItem("biography", JSON.stringify({"overview": row[0], "bio":row[1]}));
        }
    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

/**
 * request for talkpage
 */
//you may not able to understand this part at first sight. Don't worry, just be patient
function talkpage1() {
    gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: homepage,
        range: 'talk!A2:D',
    }).then(function(response) {
        var range = response.result;
        var date=[];
        var theme=[];
        var location=[];
        var section1=[];
        var year=[];
        var a=0;
        var newest=range.values[0][3];
        var oldest=range.values[range.values.length-1][3];
        var whole_section1=[];
        var whole_section2="";
        for(var y = newest; y >oldest-1 ; y--){
        whole_section1[a]="<div class='col-md-1 col-md-offset-3  col-sm-1 col-sm-offset-3 col-xs-2 col-xs-offset-1 '><p class='year'>"+y+"</p></div><div class='col-md-8 col-sm-8 col-xs-8' style='margin-bottom: 20px'>";
           for(i=0;i<range.values.length;i++){
              if (range.values[i][3]==y) {
                 var row=range.values[i];
                 date[i]=row[0];
                 theme[i]=row[1];
                 location[i]=row[2];
                 year[i]=row[3];
                 section1[i]="<p class='date2' style='margin-top: 5px;'>"+date[i]+"</p><p class='talkstheme' style='margin-left:5px;display: inline-block;'>"+theme[i]+"</p><p class='location' style='display: inline-block;margin-left:5px;' >"+location[i]+"</p><br>";
                 whole_section1[a]+=section1[i];
                }
              }
             whole_section1[a] = whole_section1[a] +"</div>";
             whole_section2+=whole_section1[a];
             a++;
            }


        document.getElementById("talkpage").innerHTML=whole_section2;
        // document.getElementById("talkpage1").innerHTML=whole_section2;
        localStorage.setItem("talkpage",JSON.stringify([date,theme,location,year]));

    }, function(response) {
        appendPre('Error: ' + response.result.error.message);
    });
}

/**
 * Append a pre element to the body containing the given message
 * as its text node.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
    var pre = document.getElementById('output');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}